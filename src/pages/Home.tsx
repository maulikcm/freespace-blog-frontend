import { useEffect, useState } from 'react';
import Post from '../components/Post';
import { IBlog } from '../interface';
import { getAllPublishedBlogs } from '../services/blogs';

export default function Home() {
  const [posts, setPosts] = useState<IBlog[] | []>([]);
  useEffect(() => {
    getAllPublishedBlogs()
      .then(({ data }: any) => {
        setPosts(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  return (
    <>
      <div>
        <h2 className='text-3xl tracking-tight font-extrabold text-gray-900 sm:text-4xl'>
          Recent publications
        </h2>
        <p className='mt-3 text-xl text-gray-500 sm:mt-4'>
          You can create your own posts by logging in, if not already.
        </p>
      </div>
      <div className='mt-6 pt-10 grid gap-16 lg:grid-cols-2 lg:gap-x-5 lg:gap-y-12'>
        {posts.map((post) => (
          <Post key={post.title} post={post} oweners={false} />
        ))}
      </div>
    </>
  );
}

import { useEffect } from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useForm } from 'react-hook-form';
import { Link, useNavigate } from 'react-router-dom';
import { auth, signup } from '../services/firebase';

type Inputs = {
  email: string;
  password: string;
  name: string;
};

export default function Register() {
  const [user, loading] = useAuthState(auth);
  const navigate = useNavigate();

  useEffect(() => {
    if (loading) {
      return;
    }
    if (user) {
      navigate('/myBlogs');
    }
  }, [user, loading]);

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<Inputs>();

  return (
    <>
      <div className='min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8'>
        <div className='max-w-md w-full space-y-8'>
          <div>
            <h2 className='mt-6 text-center text-3xl font-extrabold text-gray-900'>
              Register to create blogs
            </h2>
            <p className='mt-2 text-center text-sm text-gray-600'>
              Or{' '}
              <Link to='/login' className='link'>
                Login if already registered.
              </Link>
            </p>
          </div>
          <form className='mt-8 space-y-6' onSubmit={handleSubmit(signup)}>
            <div className='rounded-md shadow-sm'>
              <div>
                <label htmlFor='email-address'>Email</label>
                <input
                  id='email-address'
                  type='email'
                  autoComplete='email'
                  required
                  className='form-input'
                  placeholder='Email address'
                  {...register('email', { required: true })}
                />
                {errors.email && (
                  <span className='text-sm text-red'>
                    This field is required
                  </span>
                )}
              </div>
              <div>
                <label htmlFor='password'>Password</label>
                <input
                  id='password'
                  type='password'
                  autoComplete='current-password'
                  required
                  className='form-input'
                  placeholder='Password'
                  {...register('password', { required: true, minLength: 6 })}
                />
                {errors.password && (
                  <span className='text-sm text-red'>
                    This field must be 6 characters long
                  </span>
                )}
              </div>
            </div>

            <div>
              <button type='submit' className='btn btn-primary btn-block'>
                <span className='absolute left-0 inset-y-0 flex items-center pl-3'></span>
                Register
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

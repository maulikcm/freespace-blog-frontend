import { useEffect, useState } from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from '../services/firebase';
import { useNavigate, useParams } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { createBlog, getBlog, updateBlog } from '../services/blogs';
import { IBlog } from '../interface';

type Inputs = {
  content: string;
  title: string;
};

export default function Form() {
  let { id } = useParams();
  const [post, setPost] = useState<IBlog | Inputs>({ title: '', content: '' });
  useEffect(() => {
    getBlog(id as string).then(({ data }: any) => {
      setPost(data);
    });
  }, [id]);
  const [user, loading] = useAuthState(auth);
  const navigate = useNavigate();
  const [apiError, setApiError] = useState<boolean>();
  useEffect(() => {
    if (loading) {
      return;
    }
    if (!user) navigate('/login');
  }, [user, loading]);

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<Inputs>();

  const onSubmit = (data: IBlog) => {
    setApiError(false);
    if (id) {
      updateBlog(data, id)
        .then((res) => {
          navigate('/myBlogs');
        })
        .catch((error) => {
          setApiError(true);
        });
    } else {
      createBlog(data)
        .then((res) => {
          navigate('/myBlogs');
        })
        .catch((error) => {
          setApiError(true);
        });
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className='relative'>
      <div className='absolute top-0 inset-x-px'>
        <div className='border-b border-gray-200 px-2 py-2 flex justify-between items-center space-x-3 sm:px-3'>
          <h2 className='front-semibold text-xl'>
            {id ? 'Edit post' : 'Create new post'}
          </h2>
          <button type='submit' className='inline-flex btn btn-success'>
            Save
          </button>
        </div>
      </div>
      <div className='h-full border border-gray-300 rounded-lg shadow-sm overflow-hidden'>
        <div aria-hidden='true'>
          <div className='h-px' />
          <div className='py-2'>
            <div className='py-px'>
              <div className='h-9' />
            </div>
          </div>
        </div>
        <label htmlFor='title' className='sr-only'>
          Title
        </label>
        <input
          type='text'
          id='title'
          className='block p-3 w-full border-0 pt-2.5 text-lg font-medium placeholder-gray-500 focus:ring-0'
          placeholder='Post Title'
          defaultValue={post.title}
          {...register('title', { required: true })}
        />
        <label htmlFor='description' className='sr-only'>
          Description
        </label>
        <textarea
          rows={6}
          id='content'
          className='block p-3 h-full w-full border-0 py-0 resize-none placeholder-gray-500 focus:ring-0 sm:text-sm'
          placeholder='Write a post content...'
          defaultValue={post.content}
          {...register('content', { required: true })}
        />
      </div>
      {(errors.content || errors.title) && (
        <span className='text-sm text-red'>Fill the required field</span>
      )}
      {apiError && (
        <span className='text-sm text-red'>
          Something went wrong while saving.
        </span>
      )}
    </form>
  );
}

import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useParams } from 'react-router-dom';
import { IBlog } from '../interface';
import { getBlog, postComment } from '../services/blogs';

type Inputs = {
  content: string;
  email: string;
};

export default function View() {
  let { id } = useParams();
  const [post, setPost] = useState<IBlog>();
  useEffect(() => {
    getBlog(id as string).then(({ data }: any) => {
      setPost(data);
    });
  }, [id]);

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<Inputs>();

  const sendComment = (data: Inputs) => {
    postComment({ ...data, blogId: id as string }).then((res) => {
      getBlog(id as string).then(({ data }: any) => {
        setPost(data);
      });
    });
  };

  return (
    <div className='relative px-4 sm:px-6 lg:px-8'>
      <div className='text-lg max-w-prose mx-auto'>
        <h3 className='mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl'>
          {post && post.title}
        </h3>
        <h2 className='text-base text-indigo-600 font-semibold tracking-wide'>
          {post && post.user?.email}
        </h2>

        <p className='mt-8 text-xl text-gray-500 leading-8'>
          {post && post.content}
        </p>
        <hr />
        {post &&
          post.comments &&
          post.comments.map((item) => (
            <div className='p-1 border border-gray-200 rounded'>
              <p className='text-sm text-gray-500 leading-8'>
                <strong>{item.email}:</strong> {item.content}
              </p>
            </div>
          ))}
      </div>
      <div className='pt-4 max-w-2xl mx-auto'>
        <form onSubmit={handleSubmit(sendComment)} className='relative'>
          <div className='border border-gray-300 rounded-lg shadow-sm overflow-hidden focus-within:border-indigo-500 focus-within:ring-1 focus-within:ring-indigo-500'>
            <label htmlFor='comment' className='sr-only'>
              Add your comment
            </label>
            <textarea
              rows={1}
              id='content'
              className='p-3 block w-full py-3 border-0 resize-none focus:ring-0 sm:text-sm'
              placeholder='Add your comment...'
              defaultValue={''}
              {...register('content', { required: true })}
            />
            <label htmlFor='content' className='sr-only'>
              Email
            </label>
            <input
              type='email'
              id='email'
              className='p-3 block w-full py-3 border-0 resize-none focus:ring-0 sm:text-sm'
              placeholder='Your email'
              defaultValue={''}
              {...register('email', { required: true })}
            />
            <div className='py-2' aria-hidden='true'>
              <div className='py-px'>
                <div className='h-9' />
              </div>
            </div>
          </div>
          <div className='absolute bottom-0 inset-x-0 pl-3 pr-2 py-2 flex justify-end'>
            <div className='flex items-center space-x-5'>
              <div className='flex-shrink-0'>
                <button type='submit' className='btn btn-primary btn-sm'>
                  Send
                </button>
              </div>
            </div>
          </div>
          {(errors.content || errors.email) && (
            <span className='text-sm text-red-900'>
              Both the fields are required
            </span>
          )}
        </form>
      </div>
    </div>
  );
}

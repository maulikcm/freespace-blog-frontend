import { useEffect, useState } from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { Link } from 'react-router-dom';
import Post from '../components/Post';
import { IBlog } from '../interface';
import { deleteBlog, getAllBlogsByUser, publishBlog } from '../services/blogs';
import { auth } from '../services/firebase';

export default function MyBlogs() {
  const [user] = useAuthState(auth);
  const [posts, setPosts] = useState<IBlog[] | []>([]);
  useEffect(() => {
    if (user) {
      getAllBlogsByUser()
        .then(({ data }: any) => {
          setPosts(data);
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      return;
    }
  }, [user]);

  const publishPost = (id: string) => {
    publishBlog(id).then((res) => {
      getAllBlogsByUser()
        .then(({ data }: any) => {
          setPosts(data);
        })
        .catch((error) => {
          console.log(error);
        });
    });
  };

  const deletePost = (id: string) => {
    deleteBlog(id).then((res) => {
      getAllBlogsByUser()
        .then(({ data }: any) => {
          setPosts(data);
        })
        .catch((error) => {
          console.log(error);
        });
    });
  };

  return (
    <>
      <div className='my-3 flex justify-between'>
        <h2 className='front-semibold text-xl'>Your posts</h2>
        <Link to='/create' className='btn btn-outline-primary'>
          Create New
        </Link>
      </div>
      <div className='mt-6 pt-10 grid lg:px-32 gap-16 lg:grid-cols-2 lg:gap-x-5 lg:gap-y-12'>
        {posts.map((post) => (
          <Post
            key={post.title}
            post={post}
            oweners={true}
            publishPost={publishPost}
            deletePost={deletePost}
          />
        ))}
      </div>
    </>
  );
}

import axios from 'axios';
import { IUser } from '../interface';
const BASE_URL = process.env.REACT_APP_API_URL + 'user'

export const register = (data: IUser) => {
  console.log(data)
  return new Promise((resolve, reject) => {
    axios.post(BASE_URL, data).then(({ data }) => {
      if (data.success) {
        resolve(data);
      } else {
        reject(data);
      }
    }).catch(error => {
      reject({ success: false, error: error })
    })

  })
}
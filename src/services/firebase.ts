import axios from "axios";
import { initializeApp } from "firebase/app";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut } from "firebase/auth";
import { IUser } from "../interface";
import { register } from './users'

// This config should not be kept here, for the demo purpose I have left it here.
const firebaseConfig = {
  apiKey: "AIzaSyDVHBB1wggz0PGv8Gm0BxaKYU-OKOTqn78",
  authDomain: "leave-manager-6e077.firebaseapp.com",
  projectId: "leave-manager-6e077",
  storageBucket: "leave-manager-6e077.appspot.com",
  messagingSenderId: "50248291253",
  appId: "1:50248291253:web:3702f20863b631e0647ffc",
  measurementId: "G-KF605TL6M4"
};

initializeApp(firebaseConfig);
export const auth = getAuth();

export const signup = async ({ email, password }: IUser) => {
  createUserWithEmailAndPassword(auth, email, password as string)
    .then((userCredential) => {
      const user = (userCredential as any).user;
      axios.defaults.headers.common['Authorization'] = user.accessToken;
      const data: IUser = {
        userId: user.uid,
        email: email
      }
      console.log('User registered successfully', user);
      register(data)
    })
    .catch((error) => {
      console.error('User register error', error)
    });
};

export const login = ({ email, password }: IUser) => {
  signInWithEmailAndPassword(auth, email, password as string)
    .then((userCredential) => {
      const user = (userCredential as any).user;
      axios.defaults.headers.common['Authorization'] = user.accessToken;
      console.log('User logged in successfully', user);
    })
    .catch((error) => {
      console.error('User login error', error)
    });

}

export const logout = () => {
  signOut(auth).then(() => {
    console.log('user logged out')
  }).catch((error) => {
    console.error('User logout error', error)
  });
}
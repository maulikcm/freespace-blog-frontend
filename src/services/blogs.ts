import axios from 'axios';
import { IBlog, IComment } from '../interface';
const BASE_URL = process.env.REACT_APP_API_URL + 'blog';

export const createBlog = (data: IBlog) => {
  return new Promise((resolve, reject) => {
    axios
      .post(BASE_URL, data)
      .then(({ data }) => {
        if (data.success) {
          resolve(data);
        } else {
          reject(data);
        }
      })
      .catch((error) => {
        reject({ success: false, error: error });
      });
  });
};

export const updateBlog = (data: IBlog, id: string) => {
  return new Promise((resolve, reject) => {
    axios
      .patch(BASE_URL, { id, data })
      .then(({ data }) => {
        if (data.success) {
          resolve(data);
        } else {
          reject(data);
        }
      })
      .catch((error) => {
        reject({ success: false, error: error });
      });
  });
};

export const postComment = (data: IComment) => {
  return new Promise((resolve, reject) => {
    axios
      .post(BASE_URL + '/comment', data)
      .then(({ data }) => {
        if (data.success) {
          resolve(data);
        } else {
          reject(data);
        }
      })
      .catch((error) => {
        reject({ success: false, error: error });
      });
  });
};

export const getAllPublishedBlogs = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(BASE_URL + '/published')
      .then(({ data }) => {
        if (data.success) {
          resolve(data);
        } else {
          reject(data);
        }
      })
      .catch((error) => {
        reject({ success: false, error: error });
      });
  });
};

export const getAllBlogsByUser = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(BASE_URL)
      .then(({ data }) => {
        if (data.success) {
          resolve(data);
        } else {
          reject(data);
        }
      })
      .catch((error) => {
        reject({ success: false, error: error });
      });
  });
};

export const getBlog = (id: string) => {
  return new Promise((resolve, reject) => {
    axios
      .get(BASE_URL + '/' + id)
      .then(({ data }) => {
        if (data.success) {
          resolve(data);
        } else {
          reject(data);
        }
      })
      .catch((error) => {
        reject({ success: false, error: error });
      });
  });
};

export const publishBlog = (id: string) => {
  return new Promise((resolve, reject) => {
    axios
      .get(BASE_URL + '/publish/' + id)
      .then(({ data }) => {
        if (data.success) {
          resolve(data);
        } else {
          reject(data);
        }
      })
      .catch((error) => {
        reject({ success: false, error: error });
      });
  });
};

export const deleteBlog = (id: string) => {
  return new Promise((resolve, reject) => {
    axios
      .delete(BASE_URL + '/' + id)
      .then(({ data }) => {
        if (data.success) {
          resolve(data);
        } else {
          reject(data);
        }
      })
      .catch((error) => {
        reject({ success: false, error: error });
      });
  });
};

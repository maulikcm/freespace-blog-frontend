export interface IUser {
  email: string;
  userId?: string;
  password?: string;
  _id?: string;
}

export interface IBlog {
  content: string;
  title: string;
  isPublished?: boolean;
  userId?: boolean;
  createdAt?: string;
  _id?: object | string;
  user?: IUser;
  comments?: IComment[];
}

export interface IComment {
  blogId: string;
  email: string;
  content: string;
}

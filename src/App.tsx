import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Layout from './components/Layout';
import Form from './pages/Form';
import Home from './pages/Home';
import Login from './pages/Login';
import MyBlogs from './pages/MyBlogs';
import View from './pages/View';
import Register from './pages/Register';

const routes = [
  {
    path: '/',
    component: <Layout view={<Home />} />
  },
  {
    path: '/:id',
    component: <Layout view={<View />} />
  },
  {
    path: '/register',
    component: <Layout view={<Register />} />
  },
  {
    path: '/login',
    component: <Layout view={<Login />} />
  },
  {
    path: '/myBlogs',
    component: <Layout view={<MyBlogs />} />
  },
  {
    path: '/create',
    component: <Layout view={<Form />} />
  },
  {
    path: '/edit/:id',
    component: <Layout view={<Form />} />
  }
];

const App = () => {
  return (
    <>
      <Router>
        <Routes>
          {routes.map((route, i) => (
            <Route key={i} path={route.path} element={route.component} />
          ))}
          {/* <Route key="404" children={<NotFound />} /> */}
        </Routes>
      </Router>
    </>
  );
};

export default App;

import { Link, useLocation } from 'react-router-dom';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useEffect, useState } from 'react';
import { auth, logout } from '../services/firebase';
import { IUser } from '../interface';
import axios from 'axios';

export default function Nav() {
  const [user, loading] = useAuthState(auth);
  const [userData, setUserData] = useState<IUser | null>();
  useEffect(() => {
    if (loading) {
      return;
    }
    if (user) {
      const data: IUser = {
        email: user.email as string,
        userId: user.uid as string
      };
      axios.defaults.headers.common['Authorization'] = (
        user as any
      ).accessToken;

      setUserData(data);
    }
  }, [user, loading]);
  const location = useLocation();
  const handleLogout = () => {
    logout();
    axios.defaults.headers.common['Authorization'] = '';
    setUserData(null);
  };
  return (
    <header className='bg-indigo-600'>
      <nav className='max-w-7xl mx-auto px-4 sm:px-6 lg:px-8' aria-label='Top'>
        <div className='w-full py-6 flex items-center justify-between border-b border-indigo-500 lg:border-none'>
          <div className='flex items-center'>
            <Link to='/'>
              <h1 className='text-white font-bold text-xl'>
                Freespace | Blogs
              </h1>
            </Link>
          </div>
          {userData ? (
            <div className='flex ml-10 space-x-4'>
              <Link to='/myBlogs' className='text-white pt-2 hover:underline'>
                {userData.email}
              </Link>
              <button
                type='button'
                className='btn btn-primary btn-primary-transperent inline-block'
                onClick={handleLogout}
              >
                Logout
              </button>
            </div>
          ) : (
            <div className='flex ml-10 space-x-4'>
              {location.pathname !== '/login' && (
                <Link
                  to='/login'
                  className='btn btn-primary btn-primary-transperent inline-block'
                >
                  Login
                </Link>
              )}
              {location.pathname !== '/register' && (
                <Link to='/register' className='btn btn-primary inline-block'>
                  Sign up
                </Link>
              )}
            </div>
          )}
        </div>
      </nav>
    </header>
  );
}

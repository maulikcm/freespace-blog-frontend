import { Link } from 'react-router-dom';
import { IBlog } from '../interface';

interface postProps {
  post: IBlog;
  oweners: boolean;
  publishPost?: Function;
  deletePost?: Function;
}

export default function Post({ post, oweners, publishPost, deletePost }: postProps) {
  return (
    <div className='p-3 rounded -md shadow-lg'>
      <p className='text-sm text-gray-500'>
        <time dateTime={post.createdAt}>
          {new Date(post.createdAt as string).toDateString()}
        </time>
      </p>
      <div className='mt-2 block'>
        <p className='text-xl font-semibold text-gray-900'>{post.title}</p>
        <p className='mt-3 text-base text-gray-500 truncate'>{post.content}</p>
      </div>
      <p className='pt-4 text-sm text-gray-600'>
        By <strong>{post.user?.email}</strong>
      </p>
      <div className='mt-3 flex justify-end items-center space-x-3'>
        {!oweners ? (
          <Link
            to={`/${post._id}`}
            className='text-base font-semibold text-indigo-600 hover:text-indigo-500'
          >
            Read full story
          </Link>
        ) : (
          <div>
            {post.isPublished ? (
              <div className='inline-flex justify-between items-center'>
                <Link
                  to={`/${post._id}`}
                  className='text-base font-semibold text-indigo-600 hover:text-indigo-500'
                >
                  Read full story
                </Link>
                <button
                  type='button'
                  className='inline-flex btn btn-danger btn-sm mx-2'
                  onClick={() => (deletePost as Function)(post._id as string)}
                >
                  Remove
                </button>
              </div>
            ) : (
              <>
                <button
                  type='button'
                  className='inline-flex btn btn-success btn-sm mx-2'
                  onClick={() => (publishPost as Function)(post._id as string)}
                >
                  Publish
                </button>
                <Link
                  to={`/edit/${post._id}`}
                  className='inline-flex btn btn-outline btn-sm mx-2'
                >
                  Edit
                </Link>
              </>
            )}
          </div>
        )}
      </div>
    </div>
  );
}

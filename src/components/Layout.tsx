import Nav from './Nav';

interface LayoutProps {
  view: JSX.Element;
}

const Layout = ({ view }: LayoutProps) => {
  return (
    <>
      <Nav />
      <div className='bg-white pt-6 pb-20 px-4 sm:px-6 lg:pb-28 lg:px-8'>
        <div className='relative max-w-lg mx-auto divide-y-2 divide-gray-200 lg:max-w-7xl'>
          {view}
        </div>
      </div>
    </>
  );
};

export default Layout;
